class slHTTP {
    //makes a GET Request
    //jabh tak promise resolve nhi hota toh woh unresolved state mai raheta hai
    //jabh woo resolve ho jahata hai toh then function ke pass jahata hai
    get (url) {
        return new Promise( (resolve, reject) => {
            fetch (url)
                .then(res => {
                    if(!res.ok)
                        throw new Error(res.status);
                    return res.json();
                })
                .then(data => resolve(data))
                .catch(err => reject(err));
        })
    }

    //make a POST method
    post (url, data) {
        return new Promise( (resolve, reject) =>{
            fetch(url, {
                method: 'POST',
                headers: {
                    'Content-type': 'application/json'
                },
                body: JSON.stringify(data)
            })
                .then(res => {
                    if(!res.ok)
                        throw new Error(res.status);
                    return res.json();
                })
                .then(data => resolve(data))
                .catch(err => reject(err));
        })
    }

    //make a PUT method
    put (url, data) {
        return new Promise( (resolve, reject) =>{
            fetch(url, {
                method: 'PUT',
                headers: {
                    'Content-type': 'application/json'
                },
                body: JSON.stringify(data)
            })
                .then(res => {
                    if(!res.ok)
                        throw new Error(res.status);
                    return res.json();
                })
                .then(data => resolve(data))
                .catch(err => reject(err));
        })
    }

    //make a DELETE method
    delete (url) {
        return new Promise( (resolve, reject) =>{
            fetch(url, {
                method: 'DELETE',
                headers: {
                    'Content-type': 'application/json'
                },
            })
                .then(res => {
                    if(!res.ok)
                        throw new Error(res.status);
                    return res.json();
                })
                .then(() => resolve('Resource Deleted!...'))
                .catch(err => reject(err));
        })
    }

    //delete method does not returns any thing
}
const http=new slHTTP();
export default http;