class UI
{
    constructor()
    {
        this.post_wrapper=document.getElementById('posts');
    }
    
    showPosts(posts)
    {
        let output = "";
        posts.forEach(function(post)
        {
            output+=`
            <div class="card mb-3">
            <div class="card-body">
            <h5 class="card-title">${post.title}</h5>
            <p class="card-text">${post.body}</p>
    <a href="#" class="card-link">${post.author}</a>
  </div>
</div>

            `
        });
        this.post_wrapper.innerHTML=output;
        
    }
}

const ui=new UI();
export default ui;