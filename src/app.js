import http from "./slHttp";
import ui from "./UI";

document.addEventListener('DOMContentLoaded',fetchPosts);
document.getElementById("add_post_btn").addEventListener("click",addPost);

function fetchPosts()
{
    http.get('http://localhost:3000/posts')
    .then(posts=> ui.showPosts(posts))
    .catch(err=> console.log(err));
}



function addPost()
{
    const post_title = document.getElementById('post_title').value;
    const post_body = document.getElementById('post_body').value;
    const post_author = document.getElementById('post_author').value;
    
    const data={
        title:post_title,
        body:post_body,
        author:post_author
    };
    /*
    
    const title = document.getElementById('post_title').value;
    const body = document.getElementById('post_body').value;
    const author = document.getElementById('post_author').value
    
    const data={
        title,
        body,
        author
    }
    */    
    http.post("http://localhost:3000/posts",data)
    .then(data=>fetchPosts())
    .catch(err=>console.log(err));
}