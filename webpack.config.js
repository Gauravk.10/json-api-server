const path = require('path');

module.exports = {
    entry:{
        app: ['@babel/polyfill','./src/app.js']
    },
    output:{
        path: path.resolve(__dirname,'dist'),
        filename: 'app.bundle.js'
    },
    module:{
        rules:[
            {
                test:/\.js$/,
                exclude:/(node_modules)/,
                use:{
                    loader:'babel-loader',
                    options:{
                        presets:['@babel/preset-env']
                    }
                }
            }
        ]
       
    },
     devServer:{
         publicPath:'/dist/',
         contentBase: path.resolve(__dirname),
         watchContentBase:true,
         open:true,
         hot:true
         
     }
}